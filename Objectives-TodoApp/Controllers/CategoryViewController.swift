//
//  CategoryViewController.swift
//  Objectives-TodoApp
//
//  Created by Samat Murzaliev on 24.04.2022.
//

import UIKit
import CoreData
import ChameleonFramework

class CategoryViewController: UITableViewController {
    
    var categoriesArray = [ObjectiveCategory]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        loadCategories()
        setNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavBar()
    }
    
    //MARK: - Nav bar settings
    
    private func setNavBar() {
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: ContrastColorOf(UIColor.flatTeal(), returnFlat: true)]
        navigationController?.navigationBar.backgroundColor = UIColor.flatTeal()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
        let color = UIColor(hexString: categoriesArray[indexPath.row].backColor ?? "4D4C7D")
        cell.textLabel?.text = categoriesArray[indexPath.row].name
        cell.backgroundColor = color
        cell.textLabel?.textColor = ContrastColorOf(color!, returnFlat: true)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height / 17
    }
    
    // MARK: - Table view delegate methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToObjectives", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToObjectives" {
            let destVC = segue.destination as! ObjectivesViewController
            if let indexPath = tableView.indexPathForSelectedRow {
                destVC.selectedCategory = categoriesArray[indexPath.row]
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            context.delete(categoriesArray[indexPath.row])
            categoriesArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    //MARK: - Nav bar add button
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        var textField = UITextField()
        let alert = UIAlertController(title: "Add New Category", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Add", style: .default) { action in
            let newCategory = ObjectiveCategory(context: self.context)
            newCategory.name = textField.text
            newCategory.backColor = UIColor.randomFlat().hexValue()
            self.categoriesArray.append(newCategory)
            self.saveCategories()
        }
        alert.addTextField { field in
            textField = field
            field.placeholder = "Enter New Category"
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Saving and loading Categories
    
    func saveCategories() {
        do {
            try context.save()
        } catch {
            print("Error while saving categories \(error)")
        }
        tableView.reloadData()
    }
    
    func loadCategories() {
        let request: NSFetchRequest<ObjectiveCategory> = ObjectiveCategory.fetchRequest()
        
        do {
            categoriesArray = try context.fetch(request)
        } catch {
            print("Error while loading categories \(error)")
        }
        tableView.reloadData()
    }
}

extension CategoryViewController: UINavigationBarDelegate {
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
      return .topAttached
    }
}
