//
//  ObjectivesViewController.swift
//  Objectives-TodoApp
//
//  Created by Samat Murzaliev on 24.04.2022.
//

import UIKit
import CoreData
import ChameleonFramework

class ObjectivesViewController: UITableViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var objectivesArray = [Objective]()
    var selectedCategory : ObjectiveCategory? {
        didSet{
            loadObjectives()
        }
    }
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavBar()
    }
    
    //MARK: - Nav bar settings
    
    private func setNavBar() {
        title = selectedCategory!.name
        if let colorHex = selectedCategory?.backColor {
            navigationController?.navigationBar.backgroundColor = UIColor(hexString: colorHex)
            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: ContrastColorOf(UIColor(hexString: colorHex)!, returnFlat: true)]
            navigationController?.navigationBar.tintColor = ContrastColorOf(UIColor(hexString: colorHex)!, returnFlat: true)
            navigationController?.navigationItem.rightBarButtonItem?.tintColor = ContrastColorOf(UIColor(hexString: colorHex)!, returnFlat: true)
            searchBar.backgroundColor = UIColor(hexString: colorHex)
            searchBar.searchTextField.borderStyle = .none
            searchBar.searchTextField.textColor = .black
            searchBar.searchTextField.layer.cornerRadius = 10
            searchBar.searchTextField.backgroundColor = .white
        }
    }
    
    //MARK: - Add new item bar button
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Add New Objective", message: "", preferredStyle: .alert)
        var textField = UITextField()
        let action = UIAlertAction(title: "Add", style: .default) { action in
            let newObjective = Objective(context: self.context)
            newObjective.title = textField.text
            newObjective.status = false
            newObjective.parentCategory = self.selectedCategory
            self.objectivesArray.append(newObjective)
            self.saveObjectives()
        }
        alert.addAction(action)
        alert.addTextField { field in
            field.placeholder = "Enter New Objective"
            textField = field
        }
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectivesArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ObjectiveItemCell", for: indexPath)
        let objective = objectivesArray[indexPath.row]
        cell.textLabel?.text = objective.title
        cell.accessoryType = objective.status ? .checkmark : .none
        if let color = UIColor(hexString: selectedCategory!.backColor! )?.darken(byPercentage: CGFloat(indexPath.row) / CGFloat(objectivesArray.count)) {
            cell.backgroundColor = color
            cell.textLabel?.textColor = ContrastColorOf(color, returnFlat: true)
            cell.tintColor = ContrastColorOf(color, returnFlat: true)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height / 17
    }
    
    //MARK: - Table view delegate methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        objectivesArray[indexPath.row].status = !objectivesArray[indexPath.row].status
        saveObjectives()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            context.delete(objectivesArray[indexPath.row])
            objectivesArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    // MARK: - Saving and loading methods
    
    func saveObjectives() {
        do {
            try context.save()
        } catch {
            print("Error while saving context \(error)")
        }
        self.tableView.reloadData()
    }
    
    func loadObjectives(with request: NSFetchRequest<Objective> = Objective.fetchRequest(), predicate: NSPredicate? = nil) {
        
        let categoryPredicate = NSPredicate(format: "parentCategory.name MATCHES %@", selectedCategory!.name!)
        if let addtionalPredicate = predicate {
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [categoryPredicate, addtionalPredicate])
        } else {
            request.predicate = categoryPredicate
        }
        do {
            objectivesArray = try context.fetch(request)
        } catch {
            print("Error fetching data from context \(error)")
        }
        tableView.reloadData()
    }
}

//MARK: - Search Bar Delegate Methods

extension ObjectivesViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let request : NSFetchRequest<Objective> = Objective.fetchRequest()
        let predicate = NSPredicate(format: "title CONTAINS[cd] %@", searchBar.text!)
        request.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        loadObjectives(with: request, predicate: predicate)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            loadObjectives()
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
}

//MARK: - Nav bar delegate

extension ObjectivesViewController: UINavigationBarDelegate {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
      return .topAttached
    }
}
